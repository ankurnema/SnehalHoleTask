package com.springboot.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.springboot.model.Cust;
import com.springboot.service.CustService;

@RestController
public class CustController {

	@Autowired
	private CustList CustList;

	@GetMapping("/customers/{custId}/clist")
	public List<Course> retrievecustid(@PathVariable String custId) {
		return CustList.clist(custId);
	}
	
	@GetMapping("/customers/{custId}/{custId}")
	public Course retrieveDetailsForcustid(@PathVariable String custId) {
		return studentService.retrieveCourse(studentId);
	}

	
	@PostMapping("/customers/{custId}")
	public ResponseEntity<Void> registercust(
			@PathVariable String custId, @RequestBody custmer newCust) {

		custmer cust = custmerService(custId);

		if (custid == null)
			return ResponseEntity.noContent().build();

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path(
				"/{id}").buildAndExpand(custmer.getcustId()).toUri();

		return ResponseEntity.created(location).build();
	}

}