package com.model;

import java.util.List;

public class Customer {
	private String id;
	private String name;
	private String productcategory;
	private String subcategory;
	private List<String> steps;

	public Customer() {

	}

	public Customer(String id, String name, String productcategory, String subcategory,List<String> steps) {
		super();
		this.id = id;
		this.name = name;
		this.productcategory=productcategory ;
		this.subcategory = subcategory;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getproductcategory() {
		return productcategory;
	}


	public String getName() {
		return name;
	}
	public String setName() {
		this.Name=Name;
	}

	public List<String> getSteps() {
		return steps;
	}

	@Override
	public String toString() {
		return String.format(
				"Course [id=%s, name=%s, productcategory=%s, subcategory=%s]", id, name,
				productcategory, subcategory);
	}

	

	

}